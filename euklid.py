def euklid(a, b):
	if b == 0:
		return (a, 1, 0)
	else:
		d, x, y = euklid(b, a % b)
		return (d, y, x - (a // b) * y)

print(euklid(128, 459))
